# About the project

**Cat Browser** is an example for [Android Kotlin Fundamentals: codelabs about getting data from the Internet](https://codelabs.developers.google.com/codelabs/kotlin-android-training-internet-data/index.html) (API, Retrofit, Moshi, Glide). The code is based on MarsRealEstate app, but instead of Mars properties, it displays cat breeds, cat images and cat facts, using [The Cat API](https://thecatapi.com/) and [Cat Facts API](https://catfact.ninja/).

The menu allows to switch between 2 modes: breeds (the main page displays the list of cat breeds, clickable for the detail page, which includes a random cat image from this breed, and a link to Wikipedia) and pictures (10 random cat pictures, clickable for the detail page which includes a random cat fact - unfortunately, most of the pictures come without the breed information, so I used cat facts to add some content to the detail page). Clicking on a detail image loads another random image (and another random fact, if appropriate). The purpose of this somewhat contrived interface was testing various API functionality.

The apk is here: [catbrowser.apk](apk/catbrowser.apk). Please don't abuse my API key ;)
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



