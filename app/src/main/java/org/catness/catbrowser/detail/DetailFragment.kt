package org.catness.catbrowser.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import org.catness.catbrowser.R

import org.catness.catbrowser.databinding.FragmentDetailBinding


class DetailFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val application = requireNotNull(activity).application
        val binding = FragmentDetailBinding.inflate(inflater)
        binding.setLifecycleOwner(this)

        val catBreed = DetailFragmentArgs.fromBundle(arguments!!).selectedBreed
        val viewModelFactory = DetailViewModelFactory(catBreed, application)
        binding.viewModel = ViewModelProvider(
            this, viewModelFactory
        ).get(DetailViewModel::class.java)

        binding.breedWiki.setOnClickListener {
            val str: String = (it as TextView).text.toString()
            Log.i(LOG_TAG,"Clicked: $str")
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(str)
            startActivity(openURL) 
        }

        return binding.root
    }


}