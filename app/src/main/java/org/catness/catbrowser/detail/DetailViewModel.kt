/*
 *  Copyright 2019, The Android Open Source Project
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.catness.catbrowser.detail

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.catness.catbrowser.network.CatBreed
import org.catness.catbrowser.network.CatApi
import org.catness.catbrowser.network.CatImage

/**
 *  The [ViewModel] associated with the [DetailFragment], containing information about the selected
 *  [MarsProperty].
 */
class DetailViewModel(
    catBreed: CatBreed,
    app: Application
) : AndroidViewModel(app) {

    // The internal MutableLiveData for the selected property
    private val _breed = MutableLiveData<CatBreed>()
    private val LOG_TAG: String = this.javaClass.simpleName

    // The external LiveData for the SelectedProperty
    val breed: LiveData<CatBreed>
        get() = _breed

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response


    private val _breedImage = MutableLiveData<CatImage>()
    val image: LiveData<CatImage>
        get() = _breedImage

    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    // Initialize the _selectedProperty MutableLiveData
    init {
        _breed.value = catBreed
        getCatImage(catBreed.id)
    }

    public fun onClickImage() {
        getCatImage(_breed.value!!.id)
    }

    private fun getCatImage(ids: String) {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request
            Log.i(LOG_TAG, "getCatImage: {$ids}")
            var getBreedImageDeferred = CatApi.retrofitService.getBreedImage(ids)
            try {
                var listResult = getBreedImageDeferred.await()
                var str = listResult.joinToString("\n\n")
                _response.value = str
                _breedImage.value = listResult[0]
                Log.i(LOG_TAG, "getCatImage Response: {$str}")
            } catch (e: Exception) {
                Log.i(LOG_TAG, "getCatImage Failure: ${e.message}")
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}

