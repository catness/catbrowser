package org.catness.catbrowser.imgdetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import org.catness.catbrowser.R

import org.catness.catbrowser.databinding.FragmentImgdetailBinding

class ImgdetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val application = requireNotNull(activity).application
        val binding = FragmentImgdetailBinding.inflate(inflater)
        binding.setLifecycleOwner(this)

        val selectedCatImage = ImgdetailFragmentArgs.fromBundle(arguments!!).selectedCatImage
        val viewModelFactory = ImgdetailViewModelFactory(selectedCatImage, application)
        binding.viewModel = ViewModelProvider(
            this, viewModelFactory
        ).get(ImgdetailViewModel::class.java)

        return binding.root


    }


}