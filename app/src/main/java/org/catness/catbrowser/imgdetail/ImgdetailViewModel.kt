/*
 *  Copyright 2019, The Android Open Source Project
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.catness.catbrowser.imgdetail

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.catness.catbrowser.network.*

/**
 *  The [ViewModel] associated with the [DetailFragment], containing information about the selected
 *  [MarsProperty].
 */
class ImgdetailViewModel(
    selectedCatImage: CatImage,
    app: Application
) : AndroidViewModel(app) {

    private val LOG_TAG: String = this.javaClass.simpleName

    // The internal MutableLiveData that stores the status of the most recent request
    private val _status = MutableLiveData<CatApiStatus>()

    // The external immutable LiveData for the request status
    val status: LiveData<CatApiStatus>
        get() = _status

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response

    private val _catImage = MutableLiveData<CatImage>()
    val image: LiveData<CatImage>
        get() = _catImage

    private val _fact = MutableLiveData<String>()
    val fact: LiveData<String>
        get() = _fact

    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    // Initialize the _selectedProperty MutableLiveData
    init {
        _catImage.value = selectedCatImage
        getCatFact()
    }

    public fun onClickImage() {
        getCatImage()
        getCatFact()
    }

    private fun getCatImage() {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request
            Log.i(LOG_TAG, "getCatImage")
            var getCatImageDeferred = CatApi.retrofitService.getImages(1)
            try {
                var listResult = getCatImageDeferred.await()
                var str = listResult.joinToString("\n\n")
                _response.value = str
                _catImage.value = listResult[0]
                Log.i(LOG_TAG, "getCatImage Response: {$str}")
            } catch (e: Exception) {
                Log.i(LOG_TAG, "getCatImage Failure: ${e.message}")
            }
        }
    }

    private fun getCatFact() {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request
            Log.i(LOG_TAG, "getCatFact")
            var getCatFactDeferred = FactApi.retrofitService.getCatFacts(1)
            try {
                _status.value = CatApiStatus.LOADING
                var listResult = getCatFactDeferred.await()
                _status.value = CatApiStatus.DONE
                var str = listResult.toString()
                _response.value = str
                _fact.value = listResult.data[0].fact
                Log.i(LOG_TAG, "getCatFact Response: {$str}")
            } catch (e: Exception) {
                _status.value = CatApiStatus.ERROR
                Log.i(LOG_TAG, "getCatFact Failure: ${e.message}")
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}

