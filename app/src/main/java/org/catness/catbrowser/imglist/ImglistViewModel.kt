package org.catness.catbrowser.imglist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.catness.catbrowser.network.CatApi
import org.catness.catbrowser.network.CatApiStatus
import org.catness.catbrowser.network.CatImage

/**
 * Created by catness on 9/19/20.
 */
class ImglistViewModel : ViewModel() {
    // The internal MutableLiveData that stores the status of the most recent request
    private val _status = MutableLiveData<CatApiStatus>()

    // The external immutable LiveData for the request status
    val status: LiveData<CatApiStatus>
        get() = _status

    private val _images = MutableLiveData<List<CatImage>>()

    // The external LiveData interface to the property is immutable, so only this class can modify
    val images: LiveData<List<CatImage>>
        get() = _images

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response

    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * Call getCats() on init so we can display status immediately.
     */
    init {
        getCats()
    }

    /**
     * Sets the value of the status LiveData to the Mars API status.
     */
    private fun getCats() {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request

            var getImagesDeferred = CatApi.retrofitService.getImages(10)
            try {
                // Await the completion of our Retrofit request
                _status.value = CatApiStatus.LOADING
                var listResult = getImagesDeferred.await()
                _status.value = CatApiStatus.DONE
                var str = listResult.joinToString("\n\n")
                _response.value = str
                _images.value = listResult
            } catch (e: Exception) {
                _status.value = CatApiStatus.ERROR
                _response.value = "Failure: ${e.message}"
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    // LiveData to handle navigation to the selected property
    private val _navigateToSelectedCat = MutableLiveData<CatImage>()
    val navigateToSelectedCat: LiveData<CatImage>
        get() = _navigateToSelectedCat

    /**
     * When the property is clicked, set the [_navigateToSelectedProperty] [MutableLiveData]
     * @param marsProperty The [MarsProperty] that was clicked on.
     */
    fun displayImgDetail(catImage: CatImage) {
        _navigateToSelectedCat.value = catImage
    }

    /**
     * After the navigation has taken place, make sure navigateToSelectedProperty is set to null
     */
    fun displayImgDetailComplete() {
        _navigateToSelectedCat.value = null
    }


}