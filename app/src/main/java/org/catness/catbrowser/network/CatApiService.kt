package org.catness.catbrowser.network

import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers
import kotlinx.coroutines.Deferred
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Query

// https://square.github.io/retrofit/

enum class CatApiStatus { LOADING, ERROR, DONE }

private const val BASE_URL =
    "https://api.thecatapi.com/v1/"

/**
 * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    //   .addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface CatApiService {
    @Headers("x-api-key: c209f850-a06b-4e12-a0a9-be905fa2473a")
    @GET("breeds")
    fun getBreeds():
    // The Coroutine Call Adapter allows us to return a Deferred, a Job with a result
            Deferred<List<CatBreed>>
    //Call<String>


    @Headers("x-api-key: c209f850-a06b-4e12-a0a9-be905fa2473a")
    @GET("images/search")
    fun getBreedImage(@Query("breed_ids") ids: String):
            Deferred<List<CatImage>>


    @Headers("x-api-key: c209f850-a06b-4e12-a0a9-be905fa2473a")
    @GET("images/search")
    fun getImages(@Query("limit") limit: Int):
            Deferred<List<CatImage>>

}

object CatApi {
    val retrofitService: CatApiService by lazy {
        retrofit.create(CatApiService::class.java)
    }
}

