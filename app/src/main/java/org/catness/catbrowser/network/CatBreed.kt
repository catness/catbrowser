package org.catness.catbrowser.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CatBreed(
        val id: String,
        val name: String,
        val description: String,
        // wiki url is not present in some entries, so we leave it empty as default, then it will be ignored
        @Json(name = "wikipedia_url") val wikiUrl: String = ""
) : Parcelable {
    // an additional property: if wiki url exists
   val wikiExists
        get() = !wikiUrl.isBlank()
}

@Parcelize
data class CatImage(
        val id: String,
        val url: String,
        val breeds: List<CatBreed>
) : Parcelable {}
