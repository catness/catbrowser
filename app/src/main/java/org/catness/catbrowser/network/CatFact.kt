package org.catness.catbrowser.network

import android.os.Parcelable
import com.squareup.moshi.Json


data class Fact(
        val fact: String,
        val length: Int
)

data class CatFact(
        val current_page: Int,
        val data: List<Fact>
)

