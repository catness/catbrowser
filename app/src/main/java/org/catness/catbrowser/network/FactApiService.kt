package org.catness.catbrowser.network

import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers
import kotlinx.coroutines.Deferred
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Query

// https://square.github.io/retrofit/

private const val BASE_URL =
    "https://catfact.ninja/"

/**
 * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    //   .addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface FactApiService {
    @GET("facts")
    fun getCatFacts(@Query("limit") limit: Int):
            Deferred<CatFact>

}

object FactApi {
    val retrofitService: FactApiService by lazy {
        retrofit.create(FactApiService::class.java)
    }
}

