package org.catness.catbrowser.overview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.catness.catbrowser.network.CatBreed
import org.catness.catbrowser.databinding.ListItemBreedBinding

class OverviewAdapter(private val onClickListener: OnClickListener) : ListAdapter<CatBreed, OverviewAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(item)
        }
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemBreedBinding) : RecyclerView.ViewHolder(binding.root){

        fun bind(item: CatBreed) {
            binding.breed = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemBreedBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
  /**
     * Custom listener that handles clicks on [RecyclerView] items.  Passes the [MarsProperty]
     * associated with the current item to the [onClick] function.
     * @param clickListener lambda that will be called with the current [MarsProperty]
     */
    class OnClickListener(val clickListener: (catBreed : CatBreed) -> Unit) {
        fun onClick(catBreed : CatBreed) = clickListener(catBreed)
    }

}


class DiffCallback : DiffUtil.ItemCallback<CatBreed>() {

    override fun areItemsTheSame(oldItem: CatBreed, newItem: CatBreed): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CatBreed, newItem: CatBreed): Boolean {
        return oldItem == newItem
    }

}
