package org.catness.catbrowser.overview

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.catness.catbrowser.network.CatApi
import org.catness.catbrowser.network.CatApiStatus
import org.catness.catbrowser.network.CatBreed
import retrofit2.await

/**
 * The [ViewModel] that is attached to the [OverviewFragment].
 */
class OverviewViewModel : ViewModel() {
    private val LOG_TAG : String = this.javaClass.simpleName
    // The internal MutableLiveData that stores the status of the most recent request
    private val _status = MutableLiveData<CatApiStatus>()
    // The external immutable LiveData for the request status
    val status: LiveData<CatApiStatus>
        get() = _status

    private val _breeds = MutableLiveData<List<CatBreed>>()
    // The external LiveData interface to the property is immutable, so only this class can modify
    val breeds: LiveData<List<CatBreed>>
        get() = _breeds


    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response
  // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    /**
     * Call getCats() on init so we can display status immediately.
     */
    init {
        getCats()
    }

    /**
     * Sets the value of the status LiveData to the Mars API status.
     */
    private fun getCats() {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request

            var getBreedsDeferred = CatApi.retrofitService.getBreeds()
            try {
                // Await the completion of our Retrofit request
                  _status.value = CatApiStatus.LOADING
                var listResult = getBreedsDeferred.await()
                  _status.value = CatApiStatus.DONE
                var str = listResult.joinToString("\n\n")
                Log.i(LOG_TAG,str)
                _response.value = str
                _breeds.value = listResult
            } catch (e: Exception) {
                  _status.value = CatApiStatus.ERROR
                _response.value = "Failure: ${e.message}"
                Log.i(LOG_TAG,"Failure: ${e.message}")
            }

            /*
             var listResult = CatApi.retrofitService.getBreeds()
            _response.value = "Got ${listResult} "
            */

        }

    }

      override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

  // LiveData to handle navigation to the selected property
    private val _navigateToSelectedBreed = MutableLiveData<CatBreed>()
    val navigateToSelectedBreed: LiveData<CatBreed>
        get() = _navigateToSelectedBreed


 /**
     * When the property is clicked, set the [_navigateToSelectedProperty] [MutableLiveData]
     * @param marsProperty The [MarsProperty] that was clicked on.
     */
    fun displayBreedDetails(catBreed: CatBreed) {
        _navigateToSelectedBreed.value = catBreed
    }

    /**
     * After the navigation has taken place, make sure navigateToSelectedProperty is set to null
     */
    fun displayBreedDetailsComplete() {
        _navigateToSelectedBreed.value = null
    }


}
